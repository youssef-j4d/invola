<?php

namespace App\Exceptions;

use Exception;

class ApiValidationException extends Exception
{
    //

    final public function render(): \Illuminate\Http\JsonResponse
    {

        $data = [
            'data' => null,
            'status' => [
                'status' => false,
                'code' => $this->getCode(),
                'message' => $this->getMessage(),
            ]
        ];

        return response()->json($data,  $this->getCode() );
    }

}
