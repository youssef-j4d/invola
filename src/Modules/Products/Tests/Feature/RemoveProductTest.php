<?php

namespace Modules\Products\Tests\Feature;

use Faker\Factory as Faker;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\Traits\MerchantTestingTrait;

class RemoveProductTest extends TestCase
{

    use RefreshDatabase , DatabaseMigrations , MerchantTestingTrait;



    final public function setUp(): void
    {
        parent::setUp();
        $this->seed();
        $this->faker = Faker::create();
        $this->faker_ar = Faker::create( 'ar_SA' );
        $this->prepareMerchantUser();
        $this->prepareProduct();
    }

    final public function testMerchantRemoveProduct(): void
    {
        $this->actingAs( $this->merchant_user );
        $this->assertDatabaseHas( 'products' , [
            'id' => $this->product->id ,
        ] );

        $response = $this->post( '/api/my-products/remove/' . $this->product->id , [
            '_method' => 'DELETE'
        ] , $this->request_headers );


        $response->assertStatus(200);
        $this->jsonAssertation($response, 200);
    }
}
