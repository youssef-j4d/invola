<?php

namespace Modules\Products\Tests\Feature;

use Faker\Factory as Faker;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Modules\Products\Entities\Product;
use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\Traits\MerchantTestingTrait;

class UpdateProductTest extends TestCase
{

    use RefreshDatabase , DatabaseMigrations , MerchantTestingTrait;


    final public function setUp(): void
    {
        parent::setUp();
        $this->seed();
        $this->faker = Faker::create();
        $this->faker_ar = Faker::create( 'ar_SA' );
        $this->prepareMerchantUser();
        $this->prepareProduct();
    }

    /**
     * A basic feature test example.
     *
     * @return void
     */
    final public function testMerchantUpdateProduct(): void
    {
        $this->actingAs( $this->merchant_user );
        $response = $this->post( '/api/my-products/update/' . $this->product->id , [
            'name' => [
                'en' => $this->faker->name ,
                'ar' => $this->faker_ar->name ,
            ] ,
            'slug' => $this->faker->slug ,
            'price' => $this->faker->numberBetween( 0 , 30 ) ,
            'description' => [
                'en' => $this->faker->text ,
                'ar' => $this->faker_ar->text ,
            ] ,
            'vat' => $this->faker->numberBetween( 0 , 15 ),
            'vat_type' => 'included',
            '_method' => 'PUT'
        ] , $this->request_headers );

        $response->assertStatus(200);
        $this->jsonAssertation($response, 200);
    }

}
