<?php

namespace Modules\Products\Tests\Feature;

use Faker\Factory as Faker;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\Traits\MerchantTestingTrait;

class MyProductsTest extends TestCase
{

    use RefreshDatabase , DatabaseMigrations , MerchantTestingTrait;

    /**
     * @var \Illuminate\Database\Eloquent\Collection|\Illuminate\Database\Eloquent\Model|mixed
     */

    final public function setUp(): void
    {
        parent::setUp();
        $this->seed();
        $this->faker = Faker::create();
        $this->faker_ar = Faker::create( 'ar_SA' );
        $this->prepareMerchantUser();
        $this->seedProducts();
    }

    final public function testMyProductsList(): void
    {
        $response = $this->get('/api/my-products', headers: $this->request_headers);
        $response->assertStatus(200);
        $this->jsonAssertation($response, 200);
    }
}
