<?php

namespace Modules\Products\Tests\Feature;

use Faker\Factory as Faker;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\Traits\MerchantTestingTrait;

class AddProductTest extends TestCase
{

    use RefreshDatabase , DatabaseMigrations , MerchantTestingTrait;

    final public function setUp(): void
    {
        parent::setUp();
        $this->seed();
        $this->faker = Faker::create();
        $this->faker_ar = Faker::create( 'ar_SA' );
        $this->prepareMerchantUser();
    }

    /**
     * @author Youssef Jad <youssef.j4d@gmail.com>
     */
    final public function testMerchantAddProductVatIncluded(): void
    {
        $this->merchantAddProductVatTypeBased( 'included' );
    }

    final public function testMerchantAddProductVatExcluded(): void
    {
        $this->merchantAddProductVatTypeBased( 'excluded' );
    }

    private function merchantAddProductVatTypeBased(string $vat_type): void
    {
        // test merchant add product
        $response = $this->post( '/api/my-products/create' , [
            'name' => [
                'en' => $this->faker->name ,
                'ar' => $this->faker_ar->name ,
            ] ,
            'slug' => $this->faker->slug ,
            'price' => $this->faker->numberBetween( 0 , 30 ) ,
            'description' => [
                'en' => $this->faker->text ,
                'ar' => $this->faker_ar->text ,
            ] ,
            'vat' => $this->faker->numberBetween( 0 , 15 ) ,
            'vat_type' => $vat_type ,
        ] ,                      $this->request_headers );

        $response->assertStatus( 200 );
        $this->jsonAssertation( $response , 200 );


    }
}
