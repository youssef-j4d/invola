<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

return new class extends Migration {
    /**
     * Run the migrations.
     *
     * @return void
     */
    final public function up(): void
    {
        Schema::create( 'products' , function (Blueprint $table) {
            $table->id();
            $table->unsignedInteger( 'merchant_id' )->index();
            $table->json( 'name' );
            $table->string( 'slug' , 455 );
            $table->json( 'description' );
            $table->float( 'price' , 8 , 2 )->default( 0 )->nullable(true);
            $table->tinyInteger( 'vat' )->default( 0 );
            $table->enum( 'vat_type' , ['included' , 'excluded'] )->default( 'included' );
            $table->timestamps();
        } );
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    final public function down(): void
    {
        Schema::dropIfExists( 'products' );
    }
};
