<?php

namespace Modules\Products\Entities;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Modules\Merchants\Entities\Merchant;
use Modules\Merchants\Entities\StoreSettings;
use Modules\Products\Database\factories\ProductFactory;
use Spatie\Translatable\HasTranslations;

class Product extends Model
{
    use HasFactory , HasTranslations;

    protected $table = 'products';
    public $translatable = ['name' , 'description'];
    protected $fillable = ['merchant_id' , 'name' , 'description' , 'slug' , 'price' , 'vat' , 'vat_type'];

    final protected static function newFactory(): ProductFactory
    {
        return ProductFactory::new();
    }


    final public function scopeMyProducts(\Illuminate\Database\Eloquent\Builder $query):\Illuminate\Database\Eloquent\Builder
    {
        return $query->where( 'merchant_id' , auth()->user()->id );
    }


    final public function merchant(): \Illuminate\Database\Eloquent\Relations\BelongsTo
    {
        return $this->belongsTo( Merchant::class , 'merchant_id' , 'id' );
    }


    final public function store(): \Illuminate\Database\Eloquent\Relations\BelongsTo
    {
        return $this->belongsTo( StoreSettings::class , 'merchant_id' , 'merchant_id');
    }

    final protected static function boot(): void
    {
        parent::boot();
        static::Creating( function ($product) {
            $product->merchant_id = auth()->user()->id;
        } );
    }
}
