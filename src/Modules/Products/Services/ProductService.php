<?php

namespace Modules\Products\Services;

use Illuminate\Support\Str;
use Modules\Products\Entities\Product;
use Modules\Products\Http\Requests\CreateProductRequest;
use Modules\Products\Http\Requests\UpdateProductRequest;

class ProductService
{


    public $model = null;

    public function __construct(Product $model = null)
    {
        $this->model = $model ?? new Product();
    }

    /**
     * @author Youssef Jad <youssef.j4d@gmail.com>
     */
    final public function create(CreateProductRequest $request): ?Product
    {
        return $this->storeData($request);
    }

    final public function update(UpdateProductRequest $request): ?Product
    {
        return $this->storeData($request);
    }

    /**
     * @author Youssef Jad <youssef.j4d@gmail.com>
     */
    final public function storeData(mixed $request): ?Product
    {
        $this->model->setTranslations( 'name' , $request->name );
        $this->model->setTranslations( 'description' , [$request->description] );
        $this->model->slug = Str::slug( $request->slug );
        $this->model->price = $request->price;
        $this->model->vat = $request->vat;
        $this->model->vat_type = $request->vat_type;
        $this->model->save();
        return $this->model;
    }

    final public function remove(): void
    {
        $this->model->delete();
    }

    final public function getMyProducts() :\Illuminate\Contracts\Pagination\LengthAwarePaginator
    {
        return $this->model->myProducts()->paginate( 10 );
    }

    final public function paginatedProducts(): \Illuminate\Contracts\Pagination\LengthAwarePaginator
    {
        return $this->model
            ->with( 'merchant' , 'store' )
            ->paginate( 10 );
    }


}
