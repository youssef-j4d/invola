<?php

namespace Modules\Products\Http\Controllers;

use Illuminate\Contracts\Support\Renderable;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use Modules\Products\Entities\Product;
use Modules\Products\Http\Requests\UpdateProductRequest;
use Modules\Products\Services\ProductService;

class RemoveProduct extends Controller
{
    private ProductService $products;

    public function __construct()
    {
        $this->products = new ProductService();
    }

    final public function __invoke(Product $product): JsonResponse
    {
        $this->products->model = $product;
        $this->products->remove();
        return response()->json( ['data' => null , 'status' => ['status' => true,'code' => 200 , 'message' => 'Product removed successfully']] );
    }
}
