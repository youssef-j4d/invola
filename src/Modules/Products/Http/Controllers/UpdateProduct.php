<?php

namespace Modules\Products\Http\Controllers;

use Illuminate\Contracts\Support\Renderable;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use Modules\Products\Entities\Product;
use Modules\Products\Http\Requests\UpdateProductRequest;
use Modules\Products\Services\ProductService;
use Modules\Products\Transformers\ProductResource;

class UpdateProduct extends Controller
{

    private ProductService $products;

    public function __construct()
    {
        $this->products = new ProductService();
    }

    final public function __invoke(UpdateProductRequest $request , Product $product): JsonResponse
    {
        $this->products->model = $product;
        $product = $this->products->update( $request );
        return ProductResource::make( $product )
                              ->additional( ['status' => ['status' => true,'code' => 200 , 'message' => 'Product updated successfully']] )
                              ->response()
                              ->setStatusCode( 200 );
    }

}
