<?php

namespace Modules\Products\Http\Controllers;

use Illuminate\Contracts\Support\Renderable;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use Illuminate\Support\Str;
use Modules\Products\Entities\Product;
use Modules\Products\Http\Requests\CreateProductRequest;
use Modules\Products\Services\ProductService;
use Modules\Products\Transformers\ProductResource;

class CreateProduct extends Controller
{

    private ProductService $products;

    public function __construct()
    {
        $this->products = new ProductService();
    }

    final public function __invoke(CreateProductRequest $request): JsonResponse
    {
        $product = $this->products->create( $request );
        return ProductResource::make( $product )
                              ->additional( ['status' => ['status' => true, 'code' => 200 , 'message' => 'Product created successfully']] )
                              ->response()
                              ->setStatusCode( 200 );
    }


}
