<?php

namespace Modules\Products\Http\Controllers;

use Illuminate\Contracts\Support\Renderable;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use Modules\Products\Services\ProductService;
use Modules\Products\Transformers\ProductResource;

class ProductsList extends Controller
{

    private ProductService $products;

    public function __construct()
    {
        $this->products = new ProductService();
    }

    final public function __invoke(): JsonResponse
    {
        // get my products
        return ProductResource::collection( $this->products->paginatedProducts() )
                              ->additional( ['status' => ['status' => true,'code' => 200 , 'message' => 'Products retrieved successfully']] )
                              ->response()
                              ->setStatusCode( 200 );
    }

}
