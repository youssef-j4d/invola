<?php

namespace Modules\Products\Http\Requests;

use App\Exceptions\ApiValidationException;
use Illuminate\Foundation\Http\FormRequest;
use Symfony\Component\HttpFoundation\Response;

class CreateProductRequest extends FormRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    final public function rules(): array
    {
        return [
            "name"    => "required|array",
            "name.*"  => "required|string|distinct",
            "slug"   => "required|max:450",
            "description"    => "required|array",
            "description.*"  => "required|string|distinct",
            "price"   => "required|numeric",
            "vat"   => "required|numeric",
            'vat_type'   => "required|string|in:included,excluded",
        ];
    }



    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    final public function authorize(): bool
    {
        return true;
    }
}
