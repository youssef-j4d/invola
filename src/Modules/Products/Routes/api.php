<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use Modules\Products\Http\Controllers\CreateProduct;
use Modules\Products\Http\Controllers\MyProducts;
use Modules\Products\Http\Controllers\ProductsList;
use Modules\Products\Http\Controllers\RemoveProduct;
use Modules\Products\Http\Controllers\UpdateProduct;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware( ['auth:api', 'abilities:manage-products'] )->group(function () {
    Route::get( '/my-products' , MyProducts::class );
    Route::post( '/my-products/create' , CreateProduct::class );
    Route::put( '/my-products/update/{product}' , UpdateProduct::class);
    Route::delete( '/my-products/remove/{product}' , RemoveProduct::class);
} );

Route::middleware( ['api'] )->group(function () {
    Route::get( '/products' , ProductsList::class );
} );
