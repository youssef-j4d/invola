<?php

namespace Modules\Products\Transformers;

use Illuminate\Http\Resources\Json\JsonResource;

class ProductResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request
     * @return array
     */
    final public function toArray($request): array
    {
        return [
            'id' => $this->id,
            'merchant_id' => $this->merchant_id,
            'name' => $this->name,
            'slug' => $this->slug,
            'description' => $this->description,
            'price' => $this->price,
            'vat' => $this->vat,
            'vat_type' => $this->vat_type,
            'merchant_details' => $this->whenLoaded('merchant' ),
            'store' => $this->whenLoaded('store' ),

        ];
    }
}
