<?php

namespace Modules\Customers\Tests;

use App\Models\User;

trait CustomerTestTrait
{


    private function prepareCustomerUser(): void
    {
        $name = $this->faker->name;
        $email = $this->faker->email;
        $password = '123123';

        $user = User::factory()->create( [
                                             'name' => $name ,
                                             'email' => $email ,
                                             'password' => bcrypt( $password ) ,
                                         ] );
        $user->assignRole( 'consumer' );
        $user->token = $user->createToken( $user->email )->plainTextToken;
        $this->request_headers[ 'Authorization' ] = 'Bearer ' . $user->token;
        $this->customer_user = $user;
    }

}
