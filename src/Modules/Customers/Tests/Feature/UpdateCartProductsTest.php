<?php

namespace Modules\Customers\Tests\Feature;

use Faker\Factory as Faker;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Modules\Customers\Tests\CustomerTestTrait;
use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\Traits\MerchantTestingTrait;

class UpdateCartProductsTest extends TestCase
{

    use RefreshDatabase , DatabaseMigrations , MerchantTestingTrait , CustomerTestTrait;

    private \Faker\Generator $faker;
    /**
     * @var \Illuminate\Database\Eloquent\Collection|\Illuminate\Database\Eloquent\Model|mixed
     */
    public mixed $customer_user;


    final public function setUp(): void
    {
        parent::setUp();
        $this->seed();
        $this->faker = Faker::create();
        $this->faker_ar = Faker::create( 'ar_SA' );
        $this->prepareMerchantUser();
        $this->seedProducts();
    }

    /**
     * @author Youssef Jad <youssef.j4d@gmail.com>
     */
    public function testUpdateCartProduct()
    {
        $this->prepareCustomerUser();
        $this->actingAs( $this->customer_user );
        $response = $this->post('/api/customer/cart/update-cart-products/' . $this->seeded_products->random()->first()->id  , [], $this->request_headers);
        $this->assertTrue(true);
        $response->assertStatus(200);
        $response->assertJsonStructure( structure: [
                                                       'data' => [
                                                           'id' ,
                                                           'user_id' ,
                                                           'token_id' ,
                                                           'status' ,
                                                           'first_name' ,
                                                           'middle_name' ,
                                                           'last_name' ,
                                                           'mobile' ,
                                                           'email' ,
                                                           'line1',
                                                           'line2',
                                                           'city',
                                                           'province',
                                                           'country',
                                                           'created_at',
                                                           'updated_at',
                                                           'updated_at',
                                                           'cart_items' => [
                                                               '*' => [
                                                                   'id',
                                                                   'cart_id',
                                                                   'product_id',
                                                                   'price',
                                                                   'main_price',
                                                                   'vat',
                                                                   'vat_type',
                                                                   'quantity',
                                                                   'created_at',
                                                                   'updated_at',
                                                               ]
                                                           ]
                                                       ] ,
                                                       'status'

                                                   ] );
    }


    public function testUpdateCartProductWithQty()
    {
        $this->prepareCustomerUser();
        $this->actingAs( $this->customer_user );
        $response = $this->post('/api/customer/cart/update-cart-products/' . $this->seeded_products->random()->first()->id  , [
            'qty' => $this->faker->numberBetween(1,10)
        ], $this->request_headers);
        $this->assertTrue(true);
        $response->assertStatus(200);
        $response->assertJsonStructure( structure: [
                                                       'data' => [
                                                           'id' ,
                                                           'user_id' ,
                                                           'token_id' ,
                                                           'status' ,
                                                           'first_name' ,
                                                           'middle_name' ,
                                                           'last_name' ,
                                                           'mobile' ,
                                                           'email' ,
                                                           'line1',
                                                           'line2',
                                                           'city',
                                                           'province',
                                                           'country',
                                                           'created_at',
                                                           'updated_at',
                                                           'updated_at',
                                                           'cart_items' => [
                                                               '*' => [
                                                                   'id',
                                                                   'cart_id',
                                                                   'product_id',
                                                                   'price',
                                                                   'main_price',
                                                                   'vat',
                                                                   'vat_type',
                                                                   'quantity',
                                                                   'created_at',
                                                                   'updated_at',
                                                               ]
                                                           ]
                                                       ] ,
                                                       'status'

                                                   ] );
    }



}
