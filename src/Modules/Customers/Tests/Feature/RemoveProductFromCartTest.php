<?php

namespace Modules\Customers\Tests\Feature;

use Faker\Factory as Faker;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\Traits\MerchantTestingTrait;

class RemoveProductFromCartTest extends TestCase
{



    use RefreshDatabase , DatabaseMigrations , MerchantTestingTrait;

    private \Faker\Generator $faker;
    /**
     * @var \Illuminate\Database\Eloquent\Collection|\Illuminate\Database\Eloquent\Model|mixed
     */
    public mixed $customer_user;


    final public function setUp(): void
    {
        parent::setUp();
        $this->seed();
        $this->faker = Faker::create();
        $this->faker_ar = Faker::create( 'ar_SA' );
        $this->prepareMerchantUser();
        $this->seedProducts();
    }

    /**
     * @author Youssef Jad <youssef.j4d@gmail.com>
     */
    public function testRemoveProductToCart()
    {
        $this->assertTrue(true);
    }

}
