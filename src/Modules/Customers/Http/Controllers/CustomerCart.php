<?php

namespace Modules\Customers\Http\Controllers;

use Illuminate\Http\JsonResponse;
use Illuminate\Routing\Controller;
use Modules\Customers\Entities\Cart;
use Modules\Customers\Services\CartItemService;
use Modules\Customers\Services\CartService;
use Modules\Customers\Transformers\CartResource;
use Modules\Products\Entities\Product;
use Psr\Container\ContainerExceptionInterface;
use Psr\Container\NotFoundExceptionInterface;
use Symfony\Component\HttpFoundation\Response;

class CustomerCart extends Controller
{


    /**
     * @var CartService
     */
    private CartService $cartService;
    private CartItemService $cartItemService;

    public function __construct()
    {
        $this->cartService = new CartService();
        $this->cartItemService = new CartItemService();
    }


    final public function getCart():  CartResource|JsonResponse
    {
        $cart = $this->getCustomerCartFromService();
        return CartResource::make( $cart )->additional(
            [
                'status' => [
                    'status' => true ,
                    'code' => Response::HTTP_OK ,
                    'message' => 'OK' ,
                ]
            ] )->response()->setStatusCode( 200 );
    }


    /**
     * @param Product $product
     * @return CartResource
     * @throws ContainerExceptionInterface
     * @throws NotFoundExceptionInterface
     */
    final public function addProductToCart(Product $product): CartResource|JsonResponse
    {
        $qty = request()?->get( 'qty' ) ?? 1;
        $this->cartItemService->addOrUpdateProductsToCart( $this->getCustomerCartFromService() , $product , $qty );

        $cart = $this->getCustomerCartFromService();
        return CartResource::make( $cart )->additional(
            [
                'status' => [
                    'status' => true ,
                    'code' => Response::HTTP_OK ,
                    'message' => 'OK' ,
                ]
            ] )->response()->setStatusCode( 200 );

    }


    /**
     * @param Product $product
     */
    final public function removeProductFromCart(Product $product): CartResource|JsonResponse
    {
        $this->cartItemService->removeProductFromCart( $this->getCustomerCartFromService() , $product );
        // get latest Cart from database
        $cart = $this->getCustomerCartFromService();
        return CartResource::make( $cart )
                           ->additional(
                               [
                                   'status' => [
                                       'status' => true ,
                                       'code' => Response::HTTP_OK ,
                                       'message' => 'OK' ,
                                   ]
                               ] )->response()
                                  ->setStatusCode( 200 );



    }

    /**
     * @return Cart|null
     */
    private function getCustomerCartFromService(): ?Cart
    {
        return $this->cartService->getCart( auth()->user() );
    }


}
