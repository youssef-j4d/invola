<?php

namespace Modules\Customers\Http\Controllers;

use Illuminate\Routing\Controller;
use Modules\Customers\Http\Requests\CustomerLoginRequest;
use Modules\Customers\Services\CartService;
use Modules\Customers\Services\CustomerService;
use Modules\Customers\Transformers\CustomerResource;
use Modules\Merchants\Transformers\MerchantResource;
use Symfony\Component\HttpFoundation\Response;

class CustomerLogin extends Controller
{

    private CustomerService $customerService;
    private CartService $cartService;

    public function __construct()
    {
        $this->customerService = new CustomerService();
        $this->cartService = new CartService();
    }


    public function __invoke(CustomerLoginRequest $request): CustomerResource
    {
        auth()->attempt( [
                             'email' => $request->email ,
                             'password' => $request->password ,
                         ] );

        auth()->user()->token = $this->customerService->issueToken( auth()->user() );
        auth()->user()->token_id = $this->customerService->getUserToken( auth()->user() )->id;

        $this->cartService->initiateCart(auth()->user());
        return CustomerResource::make( auth()->user() )->additional(
            [
                'status' => [
                    'status' => true ,
                    'code' => Response::HTTP_OK ,
                    'message' => 'You have successfully logged in!' ,
                ]
            ] );
    }

}
