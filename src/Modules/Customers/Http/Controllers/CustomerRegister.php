<?php

namespace Modules\Customers\Http\Controllers;

use Illuminate\Routing\Controller;
use Modules\Customers\Http\Requests\CustomerRegisterRequest;
use Modules\Customers\Services\CustomerService;
use Modules\Customers\Transformers\CustomerResource;
use Symfony\Component\HttpFoundation\Response ;

class CustomerRegister extends Controller
{

    private CustomerService $customerService;

    public function __construct()
    {
        $this->customerService = new CustomerService();
    }

    public function __invoke(CustomerRegisterRequest $request)
    {

        $this->customerService->createMerchantUser( $request );
        $this->customerService->giveRole( $this->customerService->model );
        $this->customerService->model->token = $this->customerService->issueToken( $this->customerService->model );
        return CustomerResource::make( $this->customerService->model )->additional( [
                                                                                        'status' => [
                                                                                            'status' => true ,
                                                                                            'code' => Response::HTTP_CREATED ,
                                                                                            'message' => 'You have successfully Registered!' ,
                                                                                        ]
                                                                                    ] );

    }

}
