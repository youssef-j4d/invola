<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use Modules\Customers\Http\Controllers\CustomerCart;
use Modules\Customers\Http\Controllers\CustomerRegister;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

// Post Route for Registering a Customer
Route::post( '/customer/register' , CustomerRegister::class );
// Post Route for Login as Customer
Route::post( '/customer/login' , CustomerLogin::class );


Route::middleware( 'auth:api' )->group(function () {
    // Cart Routes
    Route::get( '/customer/cart' , [CustomerCart::class , 'getCart'] );
    Route::post( '/customer/cart/update-cart-products/{product}' , [CustomerCart::class , 'addProductToCart'] );
    Route::post( '/customer/cart/remove-product/{product}' , [CustomerCart::class , 'removeProductFromCart'] );
} );
