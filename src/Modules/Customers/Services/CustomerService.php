<?php

namespace Modules\Customers\Services;


use App\Models\User;
use Modules\Customers\Entities\Customer;
use Modules\Customers\Http\Requests\CustomerRegisterRequest;
use Modules\Merchants\Http\Requests\RegisterMerchantRequest;

class CustomerService
{

    public Customer $model;

    public function __construct(Customer $model = null)
    {
        $this->model = $model ?? new Customer();
    }


    /**
     * @param CustomerRegisterRequest $request
     * @return mixed
     */
    final public function createMerchantUser(CustomerRegisterRequest $request): mixed
    {
        $this->model = $this->model->create( [
                                                 'name' => $request->name ,
                                                 'email' => $request->email ,
                                                 'password' => bcrypt( $request->password ) ,
                                             ] );

        return $this->model;
    }

    /**
     * @param Customer $user
     */
    final public function giveRole(Customer $user): void
    {
        $user->assignRole( 'consumer' );
    }


    final public function issueToken(Customer|User $user): string
    {
        return $user->createToken( $user->email , ['manage-cart'] )->plainTextToken;
    }

    final public function getUserToken(Customer|User $user)
    {
        [$id , $user_token] = explode( '|' , $user->token , 2 );

        return $user->tokens()
                    ->where( 'id' , $id )
                    ->where( 'token' , hash( 'sha256' , $user_token ) )
                    ->first();
    }


}
