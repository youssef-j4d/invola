<?php

namespace Modules\Customers\Services;

use App\Models\User;
use Modules\Customers\Entities\Cart;

class CartService
{

    public Cart $model;

    public function __construct(Cart $model = null)
    {
        $this->model = $model ?? new Cart();
    }

    final public function initiateCart(User $user): Cart
    {
        $token = $this->getToken( $user );
        $this->model = $this->model->create( [
                                                 'user_id' => $user->id ,
                                                 'token_id' => $token->id ,
                                                 'status' => 'cart' ,
                                                 'first_name' => '' ,
                                                 'middle_name' => '' ,
                                                 'last_name' => '' ,
                                                 'mobile' => '' ,
                                                 'email' => $user->email ?? '' ,
                                                 'line1' => '' ,
                                                 'line2' => '' ,
                                                 'city' => '' ,
                                                 'province' => '' ,
                                                 'country' => '' ,
                                             ] );
        return $this->model;
    }

    final public function updateCart(Cart $cart , array $data): Cart
    {
        $cart->update( $data );
        return $cart;
    }

    final public function getCart(User $user): Cart|null
    {
        $token = $this->getToken( $user );

        $cart = $this->model->where( 'user_id' , $user->id )
                            ->where( 'token_id' , $token->id )
                            ->with( 'cartItems' )
                            ->first();

        if($cart === null )
        {
            return $this->initiateCart($user );
        }

        return $this->model = $this->model->where( 'user_id' , $user->id )
                           ->where( 'token_id' , $token->id )
                           ->with( 'cartItems' )
                           ->first();
    }

    /**
     * @param User $user
     */
    private function getToken(User $user)
    {
        [$id , $user_token] = explode( '|' , request()->bearerToken() , 2 );

        return $user->tokens()
                      ->where( 'id' , $id )
                      ->where( 'token' , hash( 'sha256' , $user_token ) )
                      ->first();
    }

}
