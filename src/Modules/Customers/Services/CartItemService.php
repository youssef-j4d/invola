<?php

namespace Modules\Customers\Services;

use Illuminate\Database\Eloquent\Model;
use Modules\Customers\Entities\Cart;
use Modules\Customers\Entities\CartItem;

class CartItemService
{

    public CartItem $model;

    public function __construct(CartItem $model = null)
    {
        $this->model = $model ?? new CartItem();
    }


    /**
     * @param Cart $cart
     * @param $product
     * @param int $qty
     * @return \Illuminate\Database\Eloquent\Model
     */
    public function addOrUpdateProductsToCart(Cart $cart , $product , $qty = 1): Model
    {

        if ( $product->vat_type === 'included' ) {
            // calculate product price if vat_type is included then set product price
            $product_price = $product->price * $qty;
        } else {
            // calculate product price if vat_type is excluded then calculate product price as price x vat
            $product_price = ($product->price + (($product->price * $product->vat) / 100 ))  * $qty;
        }

        return $cart->cartItems()->updateOrCreate( [
                                        'cart_id' => $cart->id ,
                                        'product_id' => $product->id ,
                                        'price' => $product_price ,
                                        'main_price' => $product->price ,
                                        'quantity' => $qty ,
                                        'vat' => $product->vat,
                                        'vat_type' => $product->vat_type,
                                    ] );
    }


    public function removeProductFromCart($cart , $product): Cart
    {
        $cart->cartItems()->where( 'product_id' , $product->id )->delete();
        return $cart;
    }

}
