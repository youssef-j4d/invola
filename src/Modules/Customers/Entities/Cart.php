<?php

namespace Modules\Customers\Entities;

use App\Models\User;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class Cart extends Model
{
    use HasFactory;

    protected $table = 'cart';

    protected $fillable = [
        'user_id',
        'token_id',
        'status',
        'first_name',
        'middle_name',
        'last_name',
        'mobile',
        'email',
        'line1',
        'line2',
        'city',
        'province',
        'country',
        'country',
        'created_at',
        'updated_at',
    ];

    protected static function newFactory()
    {
        return \Modules\Customers\Database\factories\CartFactory::new();
    }

    /**
     * @author Youssef Jad <youssef.j4d@gmail.com>
     */
    public function cartItems()
    {
        return $this->hasMany(CartItem::class , 'cart_id' , 'id');
    }

    /**
     * @author Youssef Jad <youssef.j4d@gmail.com>
     */
    public function user()
    {
        return $this->belongsTo(User::class);
    }
}
