<?php

namespace Modules\Customers\Entities;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;

/**
 * @method sum(string $string)
 */
class CartItem extends Model
{
    use HasFactory;

    protected $fillable = [
        'cart_id',
        'product_id',
        'quantity',
        'price',
        'main_price',
        'vat',
        'vat_type',
        'created_at',
        'updated_at',
    ];

    protected static function newFactory()
    {
        return \Modules\Customers\Database\factories\CartItemFactory::new();
    }
}
