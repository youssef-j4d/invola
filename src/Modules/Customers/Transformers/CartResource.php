<?php

namespace Modules\Customers\Transformers;

use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\JsonResource;
use Modules\Customers\Entities\CartItem;

/**
 * @property mixed $country
 * @property mixed $cartItems
 * @property mixed $province
 * @property mixed $city
 * @property mixed $line2
 * @property mixed $line1
 * @property mixed $email
 * @property mixed $mobile
 * @property mixed $last_name
 * @property mixed $middle_name
 * @property mixed $first_name
 * @property mixed $status
 * @property mixed $token_id
 * @property mixed $user_id
 * @property mixed $id
 */
class CartResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  Request
     * @return array
     */
    final public function toArray($request): array
    {
        return [
            'id' => $this->id,
            'user_id' => $this->user_id,
            'token_id' => $this->token_id,
            'status' => $this->status,
            'first_name' => $this->first_name,
            'middle_name' => $this->middle_name,
            'last_name' => $this->last_name,
            'mobile' => $this->mobile,
            'email' => $this->email,
            'line1' => $this->line1,
            'line2' => $this->line2,
            'city' => $this->city,
            'province' => $this->province,
            'country' => $this->country,
            'created_at' => $this->country,
            'updated_at' => $this->country,
            'cart_items' => $this->whenLoaded('cartItems' , CartItemResource::collection($this->cartItems)),
            'cart_sum_price' => $this->sumCartProducts($this->cartItems),
            'total_car_qty_items' => $this->getProductsTotalQuantity($this->cartItems),

        ];
    }

    private function sumCartProducts( $cartItems):float
    {
        return $cartItems->sum('price');
    }

    private function getProductsTotalQuantity( $cartItems):int
    {
        return $cartItems->sum('quantity');
    }

}
