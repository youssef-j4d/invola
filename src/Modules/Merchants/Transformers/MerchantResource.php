<?php

namespace Modules\Merchants\Transformers;

use Illuminate\Http\Resources\Json\JsonResource;
use Symfony\Component\HttpFoundation\Response ;

class MerchantResource extends JsonResource
{


    final public function toArray($request): array
    {
        return [
            'id' => $this->id ,
            'name' => $this->name ,
            'email' => $this->email ,
            'access_token' => $this->token ,
            'created_at' => $this->created_at ,
            'updated_at' => $this->updated_at ,
        ];
    }
}
