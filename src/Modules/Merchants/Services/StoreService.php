<?php

namespace Modules\Merchants\Services;

use App\Models\User;
use Carbon\Carbon;
use Modules\Merchants\Entities\Merchant;
use Modules\Merchants\Entities\StoreSettings;
use Modules\Merchants\Http\Requests\RegisterMerchantRequest;

class StoreService
{
    public StoreSettings $model;

    public function __construct(StoreSettings $model = null)
    {
        $this->model = $model ?? new StoreSettings();
    }


    // create or update merchant user store settings

    /**
     * @author Youssef Jad <youssef.j4d@gmail.com>
     */
    final public function createOrUpdateStore(array $inputs): StoreSettings
    {
        return $this->model = $this->model->myStore()->updateOrCreate( [
                                                                           'merchant_id' => auth()->user()->id ,
                                                                           'store_name' => $inputs[ 'store_name' ] ,
                                                                           'shipping_price' => $inputs[ 'shipping_price' ] ?? null ,
                                                                           'created_at' => Carbon::now() ,
                                                                           'updated_at' => Carbon::now() ,
                                                                       ] );
    }

    final public function getStoreSettings(): StoreSettings|null
    {
        return $this->model->myStore()->first();
    }
}
