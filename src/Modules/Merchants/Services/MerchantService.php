<?php

namespace Modules\Merchants\Services;

use App\Models\User;
use Modules\Merchants\Http\Requests\RegisterMerchantRequest;

class MerchantService
{
    public User $model;

    public function __construct(User $model = null)
    {
        $this->model = $model ?? new User();
    }


    /**
     * @param RegisterMerchantRequest $request
     * @return mixed
     */
    final public function createMerchantUser(RegisterMerchantRequest $request): mixed
    {
        $this->model = $this->model->create( [
                                                 'name' => $request->name ,
                                                 'email' => $request->email ,
                                                 'password' => bcrypt( $request->password ) ,
                                             ] );

        return $this->model;
    }

    /**
     * @param User $user
     */
    final public function giveRole(User $user): void
    {
        $user->assignRole( 'merchants' );
    }


    final public function issueToken(User $user): string
    {
        return $user->createToken( $user->email, ['manage-products' , 'manage-store'] )->plainTextToken;
    }


}
