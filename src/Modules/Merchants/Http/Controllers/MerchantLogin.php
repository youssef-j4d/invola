<?php

namespace Modules\Merchants\Http\Controllers;

use App\Models\User;
use Illuminate\Contracts\Support\Renderable;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use Modules\Merchants\Services\MerchantService;
use Modules\Merchants\Transformers\MerchantResource;
use Symfony\Component\HttpFoundation\Response;

class MerchantLogin extends Controller
{

    private MerchantService $merchantService;

    public function __construct()
    {
        $this->merchantService = new MerchantService();
    }

    public function __invoke(\Modules\Merchants\Http\Requests\LoginMerchantRequest $request): MerchantResource
    {
        auth()->attempt( [
                             'email' => $request->email ,
                             'password' => $request->password ,
                         ] );

        auth()->user()->token = $this->merchantService->issueToken( auth()->user() );
        return MerchantResource::make( auth()->user() )->additional(
            [
                'status' => [
                    'status' => true ,
                    'code' => Response::HTTP_OK ,
                    'message' => 'You have successfully logged in!' ,
                ]
            ] );
    }


}
