<?php

namespace Modules\Merchants\Http\Controllers;

use App\Models\User;
use Illuminate\Contracts\Support\Renderable;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use Modules\Merchants\Services\MerchantService;
use Modules\Merchants\Transformers\MerchantResource;
use Symfony\Component\HttpFoundation\Response;

class MerchantRegisterAction extends Controller
{

    private MerchantService $merchantService;

    public function __construct()
    {
        $this->merchantService = new MerchantService();
    }

    final public function __invoke(\Modules\Merchants\Http\Requests\RegisterMerchantRequest $request): MerchantResource
    {
        $this->merchantService->createMerchantUser( $request );
        $this->merchantService->giveRole( $this->merchantService->model );
        $this->merchantService->model->token = $this->merchantService->issueToken( $this->merchantService->model );
        return MerchantResource::make( $this->merchantService->model )->additional( [
                                                                                        'status' => [
                                                                                            'status' => true ,
                                                                                            'code' => Response::HTTP_CREATED ,
                                                                                            'message' => 'You have successfully Registered!' ,
                                                                                        ]
                                                                                    ] );
    }


}
