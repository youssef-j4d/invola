<?php

namespace Modules\Merchants\Http\Controllers;

use Illuminate\Contracts\Support\Renderable;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use Modules\Merchants\Http\Requests\MerchantStoreRequest;
use Modules\Merchants\Services\MerchantService;
use Modules\Merchants\Services\StoreService;
use Modules\Merchants\Transformers\MerchantStoreResource;
use Symfony\Component\HttpFoundation\Response;


class MerchantsController extends Controller
{

    /**
     * @var StoreService
     */
    private StoreService $storeService;

    public function __construct()
    {
        $this->storeService = new StoreService();
    }


    final public function settings(): mixed
    {
        // get Store Settings;
        $store = $this->storeService->getStoreSettings();
        if ( $store === null ) {
            return response()->json( [
                                         'data' => null ,
                                         'status' => [
                                             'status' => false ,
                                             'code' => Response::HTTP_NO_CONTENT ,
                                             'message' => 'Store Settings Not Found!, Please Update Store Settings'
                                         ]
                                     ] );
        }
        return MerchantStoreResource::make( $store )->additional( [
                                                                      'status' => [
                                                                          'status' => true ,
                                                                          'code' => Response::HTTP_OK ,
                                                                          'message' => 'Store Settings Found!'
                                                                      ]
                                                                  ] );
    }

    final public function updateSettings(MerchantStoreRequest $request): MerchantStoreResource
    {
        $this->storeService->createOrUpdateStore( $request->validated() );
        return MerchantStoreResource::make( $this->storeService->model )->additional( [
                                                                                          'status' => [
                                                                                              'status' => true ,
                                                                                              'code' => Response::HTTP_OK ,
                                                                                              'message' => 'Store Settings Created / Updated Successfully!'
                                                                                          ]
                                                                                      ] );
    }

}
