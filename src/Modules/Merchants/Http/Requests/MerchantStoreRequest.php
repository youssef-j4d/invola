<?php

namespace Modules\Merchants\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class MerchantStoreRequest extends FormRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    final public function rules() :array
    {
        return [
            'store_name' => 'required|string|max:255',
            'shipping_price' => 'numeric',
        ];
    }

}
