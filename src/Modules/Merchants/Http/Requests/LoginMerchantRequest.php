<?php

namespace Modules\Merchants\Http\Requests;

use App\Exceptions\ApiException;
use App\Exceptions\ApiValidationException;
use App\Models\User;
use Illuminate\Foundation\Http\FormRequest;
use Symfony\Component\HttpFoundation\Response;


class LoginMerchantRequest extends FormRequest
{


    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    final public function rules(): array
    {
        return [
            'email' => 'required|string|email' ,
            'password' => 'required|string|min:6' ,
        ];
    }


    /**
     * @throws ApiException
     */
    final public function passedValidation(): void
    {
        // validate the credentials
        if ( !auth()->attempt( $this->only( 'email' , 'password' ) ) ) {
            throw new ApiValidationException( 'Invalid credentials' , Response::HTTP_UNAUTHORIZED );
        }
    }


}
