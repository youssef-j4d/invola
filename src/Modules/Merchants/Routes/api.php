<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use Modules\Merchants\Http\Controllers\MerchantLogin;
use Modules\Merchants\Http\Controllers\MerchantRegisterAction;
use Modules\Merchants\Http\Controllers\MerchantsController;

// Post Route for Registering a Merchant
Route::post( '/merchants/register' , MerchantRegisterAction::class );
Route::post( '/merchants/login' , MerchantLogin::class );


Route::middleware( 'auth:api' )->group(function () {
    Route::get( '/merchants/settings' , [MerchantsController::class , 'settings'] );
    Route::post( '/merchants/update-settings' , [MerchantsController::class , 'updateSettings']);

} );
