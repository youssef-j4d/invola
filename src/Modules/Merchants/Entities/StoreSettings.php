<?php

namespace Modules\Merchants\Entities;

use App\Models\User;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class StoreSettings extends Model
{
    use HasFactory;
    protected $table = 'store_settings';

    protected $fillable = [
        'merchant_id',
        'store_name',
        'shipping_price',
        'created_at',
        'updated_at',
    ];


    final public function user(): \Illuminate\Database\Eloquent\Relations\BelongsTo
    {
        return $this->belongsTo(User::class , 'merchant_id' , 'id');
    }

    /**
     * @param $query
     * @return mixed
     */
    final public function scopeMyStore(mixed $query): mixed
    {
        return $query->where('merchant_id' , auth()->user()->id);
    }
}
