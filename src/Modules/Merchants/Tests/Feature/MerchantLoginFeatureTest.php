<?php

namespace Modules\Merchants\Tests\Feature;

use App\Models\User;
use Faker\Factory as Faker;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class MerchantLoginFeatureTest extends TestCase
{

    use RefreshDatabase , DatabaseMigrations;

    final public function setUp(): void
    {
        parent::setUp();
        $this->seed();
    }

    final public function testMerchantLogin(): void
    {
        $faker = Faker::create();
        $name = $faker->name;
        $email = $faker->email;
        $password = '123123';

        User::factory()->create( [
                                            'name' => $name ,
                                            'email' => $email ,
                                            'password' => bcrypt( $password ) ,
                                        ] );

        $headers = [
            'Accept' => 'application/json' ,
        ];

        $data = [
            'email' => $email ,
            'password' => $password ,
        ];

        $response = $this->post( '/api/merchants/login' , $data , $headers );

        $this->assertDatabaseHas( 'users' , [
            'name' => $name ,
            'email' => $email ,
        ] );

        $response->assertStatus( 200 );
        $response->assertJsonStructure( structure: [
                                                       'data' => [
                                                           'id' ,
                                                           'name' ,
                                                           'email' ,
                                                           'access_token' ,
                                                           'created_at' ,
                                                           'updated_at' ,
                                                       ] ,
                                                       'status' => [
                                                           'status' ,
                                                           'code' ,
                                                           'message' ,
                                                       ]
                                                   ] );
    }

}
