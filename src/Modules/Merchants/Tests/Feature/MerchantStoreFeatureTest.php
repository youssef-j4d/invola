<?php

namespace Modules\Merchants\Tests\Feature;

use App\Models\User;
use Faker\Factory as Faker;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\Traits\MerchantTestingTrait;

class MerchantStoreFeatureTest extends TestCase
{
    use RefreshDatabase , DatabaseMigrations , MerchantTestingTrait;

    private \Faker\Generator $faker;


    final public function setUp(): void
    {
        parent::setUp();
        $this->seed();
        $this->faker = Faker::create();
        $this->prepareMerchantUser();
    }

    final public function testMerchantGetStoreDetailsForFirstTime():void
    {
        $response = $this->get( '/api/merchants/settings' , $this->request_headers );

        $this->assertDatabaseMissing( 'store_settings' , [
            'user_id' => $this->merchant_user->id ,
        ] );

        $this->jsonAssertation( $response , status_code: 200 );
        $response->assertJson( [
                                   'data' => null ,
                               ] );
    }

    final public function testMerchantUpdateStoreDetails():void
    {
        $this->assertDatabaseMissing( 'store_settings' , [
            'user_id' => $this->merchant_user->id ,
        ] );

        $data = [
            'store_name' => $this->faker->name ,
            'shipping_price' => $this->faker->numberBetween( 0 , 30 ) ,
        ];

        $response = $this->post( '/api/merchants/update-settings' , $data , $this->request_headers );
        $this->jsonAssertation( $response , status_code: 201 );
    }

    final public function testMerchantGetStoreDetails():void
    {
        $this->testMerchantGetStoreDetailsForFirstTime();
        $this->testMerchantUpdateStoreDetails();
        $this->assertDatabaseHas( 'store_settings' , [
            'merchant_id' => $this->merchant_user->id ,
        ] );

        $response = $this->get( '/api/merchants/settings' , $this->request_headers );
        $this->jsonAssertation( $response , status_code: 200 );
    }

}
