<?php

namespace Modules\Merchants\Tests\Feature;

use App\Models\User;
use Faker\Factory as Faker;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class MerchantRegisterFeatureTest extends TestCase
{

    use RefreshDatabase , DatabaseMigrations;

    final public function setUp(): void
    {
        parent::setUp();
        $this->seed();
    }

    final public function testMerchantRegisterApiCall(): void
    {
        $faker = Faker::create();

        $headers = [
            'Accept' => 'application/json' ,
        ];

        $data = [
            'name' => $faker->name() ,
            'email' => $faker->safeEmail() ,
            'password' => $faker->password( 6 ) ,
        ];

        $response = $this->post('/api/merchants/register' , $data , $headers);

        $this->assertDatabaseHas( 'users' , [
            'name' => $data[ 'name' ] ,
            'email' => $data[ 'email' ] ,
        ] );

        $response->assertStatus(201);
        $response->assertJsonStructure( structure: [
                                                       'data' => [
                                                           'id' ,
                                                           'name' ,
                                                           'email' ,
                                                           'access_token' ,
                                                           'created_at' ,
                                                           'updated_at' ,
                                                       ],
                                                       'status' => [
                                                           'status' ,
                                                           'code' ,
                                                           'message' ,
                                                       ]
                                                   ] );
    }

}
