<?php

namespace Modules\Merchants\Tests\Unit;

use Faker\Factory as Faker;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\ResourceResponse;
use Illuminate\Testing\TestResponse;
use Modules\Merchants\Http\Controllers\MerchantRegisterAction;
use Modules\Merchants\Http\Requests\RegisterMerchantRequest;
use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class MerchantRegisterTest extends TestCase
{
    use RefreshDatabase , DatabaseMigrations;

    final public function setUp(): void
    {
        parent::setUp();
        $this->seed();
    }

    /**
     * A basic unit test example.
     *
     * @return void
     */
    final public function testRegisterMerchantFunction(): void
    {
        $faker = Faker::create();

        $headers = [
            'Accept' => 'application/json' ,
        ];

        $data = [
            'name' => $faker->name() ,
            'email' => $faker->safeEmail() ,
            'password' => $faker->password( 6 ) ,
        ];

        $request = new RegisterMerchantRequest( $data , [] , [] , [] , [] , $headers );
        $function_response = app( MerchantRegisterAction::class )( $request );

        $testRes = new TestResponse( response: $function_response->response() );

        $this->assertDatabaseHas( 'users' , [
            'name' => $data[ 'name' ] ,
            'email' => $data[ 'email' ] ,
        ] );
        $testRes->assertStatus( status: 201 );
        $testRes->assertJsonStructure( structure: [
                                                      'data' => [
                                                          'id' ,
                                                          'name' ,
                                                          'email' ,
                                                          'access_token' ,
                                                          'created_at' ,
                                                          'updated_at' ,
                                                      ] ,
                                                      'status' => [
                                                          'status' ,
                                                          'code' ,
                                                          'message' ,
                                                      ]
                                                  ] );


    }


}
