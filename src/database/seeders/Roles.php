<?php

namespace Database\Seeders;

use Carbon\Carbon;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use Spatie\Permission\Models\Role;

class Roles extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    final public function run(): void
    {
        $roles = [
            'consumer' ,
            'merchants'
        ];

        foreach ( $roles as $role ) {
            $role = Role::firstOrCreate( ['name' => $role] );
            $role->created_at = Carbon::now();
            $role->updated_at = Carbon::now();
            $role->save();
        }

    }
}
