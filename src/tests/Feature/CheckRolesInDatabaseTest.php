<?php

namespace Tests\Feature;

use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

class CheckRolesInDatabaseTest extends TestCase
{
    use DatabaseMigrations;

    public function setUp(): void
    {
        parent::setUp();
        $this->seed();
    }

    /**
     * A basic feature test example.
     *
     * @return void
     */
    final public function test_check_roles_in_database(): void
    {
        $this->assertDatabaseHas( 'roles' , [
            'name' => 'consumer' ,
        ] );

        $this->assertDatabaseHas( 'roles' , [
            'name' => 'merchants' ,
        ] );
    }
}
