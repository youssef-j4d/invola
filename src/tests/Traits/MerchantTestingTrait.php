<?php

namespace Tests\Traits;
use App\Models\User;
use Modules\Products\Entities\Product;

trait MerchantTestingTrait
{
    private array $request_headers = [
        'Accept' => 'application/json' ,
    ];
    private $product;

    private $merchant_user;


    final public function prepareMerchantUser(): void
    {
        $name = $this->faker->name;
        $email = $this->faker->email;
        $password = '123123';

        $user = User::factory()->create( [
                                             'name' => $name ,
                                             'email' => $email ,
                                             'password' => bcrypt( $password ) ,
                                         ] );
        $user->assignRole( 'merchants' );
        $user->token = $user->createToken( $user->email )->plainTextToken;
        $this->request_headers[ 'Authorization' ] = 'Bearer ' . $user->token;
        $this->merchant_user = $user;
    }

    final public function prepareProduct(): void
    {
        $this->actingAs( $this->merchant_user );
        $this->product = Product::factory()->create( [
                                                         'name' => [
                                                             'en' => $this->faker->name ,
                                                             'ar' => $this->faker_ar->name ,
                                                         ] ,
                                                         'slug' => $this->faker->slug ,
                                                         'price' => $this->faker->numberBetween( 0 , 30 ) ,
                                                         'description' => [
                                                             'en' => $this->faker->text ,
                                                             'ar' => $this->faker_ar->text ,
                                                         ] ,
                                                         'vat' => $this->faker->numberBetween( 0 , 15 ) ,
                                                         'vat_type' => 'included' ,
                                                     ] );
    }

    /**
     * @author Youssef Jad <youssef.j4d@gmail.com>
     */
    final public function seedProducts(): void
    {
        $this->actingAs( $this->merchant_user );
        $this->seeded_products = Product::factory()->count(10)->make( [

                                                         'name' => [
                                                             'en' => $this->faker->name ,
                                                             'ar' => $this->faker_ar->name ,
                                                         ] ,
                                                         'slug' => $this->faker->slug ,
                                                         'price' => $this->faker->numberBetween( 0 , 30 ) ,
                                                         'description' => [
                                                             'en' => $this->faker->text ,
                                                             'ar' => $this->faker_ar->text ,
                                                         ] ,
                                                         'vat' => $this->faker->numberBetween( 0 , 15 ) ,
                                                         'vat_type' => $this->faker->numberBetween( 0 , 1 ) === 1 ? 'included' : 'excluded' ,
                                                     ] );

        $this->seeded_products->each(function ($item, $key) {
            $item->id = ++$key;
            $item->save();
        });
    }

    /**
     * @param \Illuminate\Testing\TestResponse $response
     * @param $status_code
     */
    final public function jsonAssertation(\Illuminate\Testing\TestResponse $response , int $status_code): void
    {
        $response->assertStatus( status: $status_code );
        $response->assertJsonStructure( structure: [
                                                       'data' ,
                                                       'status' => [
                                                           'status' ,
                                                           'code' ,
                                                           'message' ,
                                                       ]
                                                   ] );
    }


}
